Archivo de pseudocodigo

Este es un algoritmo en el que existe:

- 3 roles
- 3 cargos
- 4 ubicaciones geograficas
- 3 secciones de menu
- 5 secciones de submenu

Los roles creados:

- Gerencia
- Finanzas
- Asistente

Las reglas de negocio

- Gerencia, puede ver toda la información de cualquier continente.
- Finanzas, solo puede ver informacion del menu asociado a Contabilidad y de la ubicacion geográfica del usuario.
- Asistente, solo puede ver informacion del menu asociado a Asistente y de la ubicacion geográfica del usuario.
- Cualquier elección fuera de las reglas de negocio, el algoritmo notifica y permite al usuario elegir opciones.

Para ver el funcionamiento del pseudocodigo en vivo, se recomienda ejecutarlo con pseint 

Descarga del software:
http://pseint.sourceforge.net/    
