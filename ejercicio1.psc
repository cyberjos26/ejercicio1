Algoritmo UsuarioConRoles
	Repetir
		Escribir 'Ingrese el cargo del usuario:'
		Escribir '1) Gerente 2) Contable 3) Asistente'		
		Leer usuarioCargo	
		Escribir ''
		Segun usuarioCargo  Hacer
			1:			
				Escribir 'Tiene rol: GERENCIA ---> Puede ver informaci�n de cualquier UBICACION'
				Escribir ''
				Repetir
					Escribir 'A continuaci�n vera las opciones de GERENCIA Eliga la opci�n:'
					Escribir '1) Analisis de ventas 2) Facturaci�n 3) Agenda'				
					Leer opcionMenuPrincipal
					Escribir ''
					Segun opcionMenuPrincipal  Hacer
						1:
							Repetir
								Escribir 'En ANALISIS DE VENTAS est�:'							
								Escribir '1) Productos mas vendidos'						
								Leer opcionesSubMenu						
								Segun opcionesSubMenu Hacer
									1:
										Escribir ''
										Repetir
											Escribir 'Indique de que ubicacion sacara el reporte:'
											Escribir '1) Am�rica 2) Asia 3) Europa 4) Todas'
											Leer usuarioUbicacion
											Si opcionesSubMenu=1 Y usuarioUbicacion=1 Entonces
												Escribir ''
												Escribir '<< Imprimir: PRODUCTOS MAS VENDIDOS Ubicacion: AMERICA >>'
											SiNo
												Si opcionesSubMenu=1 Y usuarioUbicacion=2 Entonces
													Escribir ''
													Escribir '<< Imprimir: PRODUCTOS MAS VENDIDOS Ubicacion: ASIA >>'
												SiNo
													Si opcionesSubMenu=1 Y usuarioUbicacion=3 Entonces
														Escribir ''
														Escribir '<< Imprimir: PRODUCTOS MAS VENDIDOS Ubicacion: EUROPA >>'
													SiNo
														Si opcionesSubMenu=1 Y usuarioUbicacion=4 Entonces
															Escribir ''
															Escribir '<< Imprimir: PRODUCTOS MAS VENDIDOS Ubicacion: TODAS LAS UBICACIONES >>'
														FinSi
													FinSi
												FinSi
											FinSi
											Escribir ''											
											Repetir
												Escribir 'Quiere un reporte de otra ubicacion?'
												Escribir  '1) Si 2) No'							
												Leer validarContinuar
												Si validarContinuar<>1 Y validarContinuar<>2  Entonces
													Escribir ""
													Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
													Escribir ""
												Fin Si
											Hasta Que 3 > validarContinuar																					
											Escribir ''
										Hasta Que validarContinuar=2							
									De Otro Modo:
										Escribir ''
										Escribir "<< Ha elegido una opcion INCORRECTA >>"
										Escribir ''
										Repetir
											Escribir "Quiere cambiar de opcion?"
											Escribir  "1) Si 2) No"								
											Leer validarContinuar	
											Si validarContinuar<>1 Y validarContinuar<>2  Entonces
												Escribir ""
												Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
												Escribir ""
											Fin Si
										Hasta Que 3 > validarContinuar		
										Escribir ''
								Fin Segun
							Hasta Que validarContinuar=2
							
						2:
							Repetir
								Escribir 'En FACTURACION est�:'
								Escribir '1) Generar facturas 2) Estado Financiero'						
								Leer opcionesSubMenu
								Escribir ''
								Segun opcionesSubMenu  Hacer
									1:
										Repetir
											Escribir 'Indique de que ubicacion har� la factura:'
											Escribir '1) Am�rica 2) Asia 3) Europa'
											Leer usuarioUbicacion
											Si opcionesSubMenu=1 Y usuarioUbicacion=1 Entonces
												Escribir ''
												Escribir '<< Generar una: FACTURA para: AMERICA >>'
											SiNo
												Si opcionesSubMenu=1 Y usuarioUbicacion=2 Entonces
													Escribir ''
													Escribir '<< Generar una: FACTURA para: ASIA >>'
												SiNo
													Si opcionesSubMenu=1 Y usuarioUbicacion=3 Entonces
														Escribir ''
														Escribir '<< Generar una: FACTURA para: EUROPA >>'
													FinSi
												FinSi
											FinSi
											Escribir ''
											Repetir
												Escribir 'Quiere hacer una FACTURA de otra ubicacion?'
												Escribir "1) Si 2) No"									
												Leer validarContinuar
												Si validarContinuar<>1 Y validarContinuar<>2  Entonces
													Escribir ""
													Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
													Escribir ""
												Fin Si
											Hasta Que 3 > validarContinuar		
											Escribir ''
										Hasta Que validarContinuar=2
									2:
										Repetir
											Escribir 'Indique de que ubicacion sacara el reporte'
											Escribir '1) Am�rica 2) Asia 3) Europa 4) Todas'									
											Leer usuarioUbicacion
											Si opcionesSubMenu=2 Y usuarioUbicacion=1 Entonces
												Escribir ''
												Escribir '<< Imprimir: ESTADO FINANCIERO Ubicacion: AMERICA >>'
											SiNo
												Si opcionesSubMenu=2 Y usuarioUbicacion=2 Entonces
													Escribir ''
													Escribir '<< Imprimir: ESTADO FINANCIERO Ubicacion: ASIA >>'
												SiNo
													Si opcionesSubMenu=2 Y usuarioUbicacion=3 Entonces
														Escribir ''
														Escribir '<< Imprimir: ESTADO FINANCIERO Ubicacion: EUROPA >>'
													SiNo
														Si opcionesSubMenu=2 Y usuarioUbicacion=4 Entonces
															Escribir ''
															Escribir '<< Imprimir: ESTADO FINANCIERO Ubicacion: TODAS LAS UBICACIONES >>'
														FinSi
													FinSi
												FinSi
											FinSi
											Escribir ''
											Repetir
												Escribir 'Quiere un reporte de otra ubicacion?'
												Escribir '1) Si 2) No'
												Leer validarContinuar
												Si validarContinuar<>1 Y validarContinuar<>2  Entonces
													Escribir ""
													Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
													Escribir ""
												Fin Si
											Hasta Que 3 > validarContinuar		
											Escribir ''
										Hasta Que validarContinuar=2
									De Otro Modo:
										Escribir ''
										Escribir "<< Ha elegido una opcion INCORRECTA >>"
										Escribir ''
										Repetir
											Escribir "Quiere cambiar de opcion?"
											Escribir  "1) Si 2) No"								
											Leer validarContinuar	
											Si validarContinuar<>1 Y validarContinuar<>2  Entonces
												Escribir ""
												Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
												Escribir ""
											Fin Si
										Hasta Que 3 > validarContinuar		
										Escribir ''
								FinSegun
							Hasta Que validarContinuar=2
						3:
							Repetir						
								Escribir 'En AGENDA est�:'
								Escribir '1) Citas'					
								Leer opcionesSubMenu
								Escribir ''
								Segun opcionesSubMenu Hacer
									1:
										Repetir
											Escribir 'Indique de que ubicacion sacara el reporte:'
											Escribir '1) Am�rica 2) Asia 3) Europa 4) Todas'
											Leer usuarioUbicacion
											Si opcionesSubMenu=1 Y usuarioUbicacion=1 Entonces
												Escribir ''
												Escribir '<< Imprimir: CITAS Ubicacion: AMERICA >>'
											SiNo
												Si opcionesSubMenu=1 Y usuarioUbicacion=2 Entonces
													Escribir ''
													Escribir '<< Imprimir: CITAS Ubicacion: ASIA >>'
												SiNo
													Si opcionesSubMenu=1 Y usuarioUbicacion=3 Entonces
														Escribir ''
														Escribir '<< Imprimir: CITAS Ubicacion: EUROPA >>'
													SiNo
														Si opcionesSubMenu=1 Y usuarioUbicacion=4 Entonces
															Escribir ''
															Escribir '<< Imprimir: CITAS Ubicacion: TODAS LAS UBICACIONES >>'
														FinSi
													FinSi
												FinSi
											FinSi
											Escribir ''
											Repetir
												Escribir 'Quiere un reporte de otra ubicacion?'
												Escribir '1) Si 2) No'											
												Leer validarContinuar
												Si validarContinuar<>1 Y validarContinuar<>2  Entonces
													Escribir ""
													Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
													Escribir ""
												Fin Si
											Hasta Que 3 > validarContinuar		
											Escribir ''
										Hasta Que validarContinuar=2
										
									De Otro Modo:
										Escribir ''
										Escribir "<< Ha elegido una opcion INCORRECTA >>"
										Escribir ''
										Repetir
											Escribir "Quiere cambiar de opcion?"
											Escribir  "1) Si 2) No"								
											Leer validarContinuar	
											Si validarContinuar<>1 Y validarContinuar<>2  Entonces
												Escribir ""
												Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
												Escribir ""
											Fin Si
										Hasta Que 3 > validarContinuar		
										Escribir ''
								Fin Segun	
							Hasta Que validarContinuar=2
						De Otro Modo:
							Escribir ''
							Escribir "<< Has elegido una opcion INCORRECTA >>"						
					FinSegun
					Escribir ''
					Repetir
						Escribir 'Quiere navegar a otras opciones de GERENCIA?'
						Escribir '1) Si 2) No'						
						Leer validarContinuar
						Si validarContinuar<>1 Y validarContinuar<>2  Entonces
							Escribir ""
							Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
							Escribir ""
						Fin Si
					Hasta Que 3 > validarContinuar		
					Escribir ''
				Hasta Que validarContinuar=2
			2:			
				Repetir
					Escribir 'Ubicaci�n geografica del usuario:'			
					Escribir ' 1) Am�rica 2) Asia 3) Europa'
					Leer usuarioUbicacion
					Si usuarioUbicacion <> 1 Y usuarioUbicacion <> 2 Y usuarioUbicacion <> 3  Entonces
						Escribir ""
						Escribir "<< Ha elegido una opcion INCORRECTA>>"
						Escribir ""
						Repetir
							Escribir "Quiere cambiar de Ubicaci�n?"
							Escribir "1) Si 2) No"				
							Leer validarContinuar
							Si validarContinuar<>1 Y validarContinuar<>2  Entonces
								Escribir ""
								Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
								Escribir ""
							Fin Si
						Hasta Que 3 > validarContinuar		
						Escribir ""
					SiNo
						Escribir ''
						Escribir 'Tiene rol: FINANZAS ---> Solo tiene acceso a la informaci�n de Facturaci�n de la UBICACION elegida'	
						Escribir ''
						Repetir
							Escribir 'A continuaci�n vera las opciones de FINANZAS Eliga la opci�n:'
							Escribir '1) Facturaci�n'			
							Leer opcionMenuPrincipal
							Segun opcionMenuPrincipal Hacer
								1:
									Escribir ''
									Repetir
										Escribir 'En FACTURACION est�:'
										Escribir '1) Generar facturas 2) Estado Financiero'				
										Leer opcionesSubMenu
										Escribir ''
										Segun opcionesSubMenu  Hacer
											1:
												Repetir
													Escribir 'Indique de cual ubicacion har� la factura:'
													Escribir '1) Am�rica 2) Asia 3) Europa'
													Leer ubicacionSubMenu
													Si usuarioUbicacion<>ubicacionSubMenu Entonces
														Escribir ''
														Escribir '<< Usted NO TIENE PERMISO para GENERAR ESTA FACTURA de esta ubicaci�n >>'
													SiNo
														Si usuarioUbicacion=1 Y ubicacionSubMenu=1 Entonces
															Escribir ''
															Escribir '<< Generar una: FACTURA para: AMERICA >>'
														SiNo
															Si usuarioUbicacion=2 Y ubicacionSubMenu=2 Entonces
																Escribir ''
																Escribir '<< Generar una: FACTURA para: ASIA >>'
															SiNo
																Si usuarioUbicacion=3 Y ubicacionSubMenu=3 Entonces
																	Escribir ''
																	Escribir '<< Generar una: FACTURA para: EUROPA >>'
																FinSi
															FinSi
														FinSi
													FinSi
													Escribir ''
													Repetir
														Escribir 'Quiere hacer una FACTURA de otra ubicacion?'
														Escribir '1) Si 2) No'							
														Leer validarContinuar
														Si validarContinuar<>1 Y validarContinuar<>2  Entonces
															Escribir ""
															Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
															Escribir ""
														Fin Si
													Hasta Que 3 > validarContinuar		
													Escribir ''
												Hasta Que validarContinuar=2
											2:
												Repetir
													Escribir 'Indique de que ubicacion sacara el reporte'
													Escribir '1) Am�rica 2) Asia 3) Europa 4) Todas'
													Leer ubicacionSubMenu
													Si usuarioUbicacion<>ubicacionSubMenu Entonces
														Escribir ''
														Escribir '<< Usted NO TIENE PERMISO para IMPRIMIR ESTE ESTADO FINANCIERO de esta ubicaci�n >>'
													SiNo
														Si usuarioUbicacion=1 Y ubicacionSubMenu=1 Entonces
															Escribir ''
															Escribir '<< Imprimir: ESTADO FINANCIERO Ubicacion: AMERICA >>'
														SiNo
															Si usuarioUbicacion=2 Y ubicacionSubMenu=2 Entonces
																Escribir ''
																Escribir '<< Imprimir: ESTADO FINANCIERO Ubicacion: ASIA >>'
															SiNo
																Si usuarioUbicacion=3 Y ubicacionSubMenu=3 Entonces
																	Escribir ''
																	Escribir '<< Imprimir: ESTADO FINANCIERO Ubicacion: EUROPA >>'
																SiNo
																	Si opcionesSubMenu=4 Y usuarioUbicacion=4 Entonces
																		Escribir ''
																		Escribir '<< Imprimir: ESTADO FINANCIERO Ubicacion: TODAS LAS UBICACIONES >>'
																	FinSi
																FinSi
															FinSi
														FinSi
													FinSi
													Escribir ''
													Repetir
														Escribir 'Quiere un reporte de otra ubicacion?'
														Escribir  '1) Si 2) No'							
														Leer validarContinuar
														Si validarContinuar<>1 Y validarContinuar<>2  Entonces
															Escribir ""
															Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
															Escribir ""
														Fin Si
													Hasta Que 3 > validarContinuar		
													Escribir ''
												Hasta Que validarContinuar=2
										     De Otro Modo
												Escribir ""
												Escribir "<< Ha elegido una opcion INCORRECTA>>"
												Escribir ""
												Repetir
													Escribir "Quiere cambiar de opcion?"
													Escribir "1) Si 2) No"
													Leer validarContinuar	
													Si validarContinuar<>1 Y validarContinuar<>2  Entonces
														Escribir ""
														Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
														Escribir ""
													Fin Si
												Hasta Que 3 > validarContinuar		
												Escribir ""
										FinSegun	
									Hasta Que validarContinuar = 2
									Escribir ''						
								De Otro Modo:
									Escribir ""	
									Escribir "<< Ha elegido una opcion INCORRECTA>>"									
							Fin Segun
							Escribir ""			
							Repetir
								Escribir "Quiere navegar a otras opciones de FINANZAS?"
								Escribir "1) Si 2) No"							
								Leer validarContinuar
								Si validarContinuar<>1 Y validarContinuar<>2  Entonces
									Escribir ""
									Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
									Escribir ""
								Fin Si
							Hasta Que 3 > validarContinuar		
							Escribir ""
						Hasta Que validarContinuar = 2	
					Fin Si
				Hasta Que validarContinuar = 2
				
			3:
				Escribir ''
				Repetir					
					Escribir 'Ubicaci�n geografica del usuario:'
					Escribir ' 1) Am�rica 2) Asia 3) Europa'
					Leer usuarioUbicacion
					Si usuarioUbicacion <> 1 Y usuarioUbicacion <> 2 Y usuarioUbicacion <> 3 Entonces
						Escribir ""
						Escribir "<< Ha elegido una opcion INCORRECTA>>"
						Escribir ""
						Repetir
							Escribir "Quiere cambiar de Ubicaci�n?"
							Escribir "1) Si 2) No"				
							Leer validarContinuar
							Si validarContinuar<>1 Y validarContinuar<>2  Entonces
								Escribir ""
								Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
								Escribir ""
							Fin Si
						Hasta Que 3 > validarContinuar			
						Escribir ""
					SiNo
						Escribir ''
						Repetir
							Escribir 'Tiene rol: ASISTENTE ---> Solo tiene acceso a la secci�n de Agenda de la UBICACION elegida'
							Escribir ''
							Escribir 'A continuaci�n vera las opciones de ASISTENTE Eliga la opci�n:'
							Escribir '1) Agenda'				
							Leer opcionMenuPrincipal
							Segun opcionMenuPrincipal Hacer
								1:
									Escribir ''
									Repetir
									Escribir 'En AGENDA est�:'
									Escribir '1) Citas'				
									Leer opcionesSubMenu
									Segun opcionesSubMenu Hacer
										1:
											Escribir ''
											Repetir
												Escribir 'Indique de que ubicacion sacara el reporte:'
												Escribir '1) Am�rica 2) Asia 3) Europa 4) Todas'
												Leer ubicacionSubMenu
												Si usuarioUbicacion<>ubicacionSubMenu Entonces
													Escribir ''
													Escribir '<< Usted NO TIENE PERMISO para IMPRIMIR LAS CITAS de esta ubicaci�n >>'
												SiNo
													Si usuarioUbicacion=1 Y ubicacionSubMenu=1 Entonces
														Escribir ''
														Escribir '<< Imprimir: CITAS Ubicacion: AMERICA >>'
													SiNo
														Si usuarioUbicacion=2 Y ubicacionSubMenu=2 Entonces
															Escribir ''
															Escribir '<< Imprimir: CITAS Ubicacion: ASIA >>'
														SiNo
															Si usuarioUbicacion=3 Y ubicacionSubMenu=3 Entonces
																Escribir ''
																Escribir '<< Imprimir: CITAS Ubicacion EUROPA >>'
															SiNo
																Si usuarioUbicacion=4 Y ubicacionSubMenu=4 Entonces
																	Escribir ''
																	Escribir '<< Imprimir: CITAS Ubicacion TODAS LAS UBICACIONES >>'
																FinSi
															FinSi
														FinSi
													FinSi
												FinSi
												Escribir ''
												Repetir
													Escribir 'Quiere un reporte de otra ubicacion?'
													Escribir '1) Si 2) No'					
													Leer validarContinuar
													Si validarContinuar<>1 Y validarContinuar<>2  Entonces
														Escribir ""
														Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
														Escribir ""
													Fin Si
												Hasta Que 3 > validarContinuar		
												Escribir ''
											Hasta Que validarContinuar=2
											Escribir ''
										De Otro Modo:
											Escribir ""
											Escribir "<< Ha elegido una opcion INCORRECTA>>"
											Escribir ""
											Repetir
												Escribir "Quiere cambiar de opcion?"
												Escribir "1) Si 2) No"
												Leer validarContinuar	
												Si validarContinuar<>1 Y validarContinuar<>2  Entonces
													Escribir ""
													Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
													Escribir ""
												Fin Si
											Hasta Que 3 > validarContinuar		
											Escribir ""
									Fin Segun
								Hasta Que validarContinuar = 2
								Escribir ''
								De Otro Modo:
									Escribir ""	
									Escribir "<< Ha elegido una opcion INCORRECTA>>"									
							Fin Segun
							Escribir ""			
							Repetir
								Escribir "Quiere navegar a otras opciones de ASISTENTE?"
								Escribir "1) Si 2) No"							
								Leer validarContinuar
								Si validarContinuar<>1 Y validarContinuar<>2  Entonces
									Escribir ""
									Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
									Escribir ""
								Fin Si
							Hasta Que 3 > validarContinuar		
							Escribir ""
						Hasta Que validarContinuar = 2				
					Fin Si	
				Hasta Que validarContinuar = 2	
			De Otro Modo:
				Escribir ''
				Escribir "<< Ha elegido un perfil INCORRECTO >>"
		FinSegun
		Escribir ''
		Repetir
			Escribir 'Quiere cambiar de usuario?'
			Escribir '1) Si 2) No'		
			Leer validarContinuar
			Si validarContinuar<>1 Y validarContinuar<>2  Entonces
				Escribir ""
				Escribir "<< Ha elegido una opcion INCORRECTA >>"																							
				Escribir ""
			Fin Si
		Hasta Que 3 > validarContinuar		
		Escribir ''
	Hasta Que validarContinuar=2
FinAlgoritmo
